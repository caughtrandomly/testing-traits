@if($errors)
	@foreach($errors->all() as $error)
		<div class="alert alert--error">{{ $error }}</div>
	@endforeach
@endif

@if(Session::get('success'))
	<div class="alert alert--success">{{ Session::get('success') }}</div>
@endif

{{ Form::open() }}

	{{ Form::text('email', '', array('placeholder' => 'email address')) }}
	{{ Form::submit('Send') }}

{{ Form::close() }}