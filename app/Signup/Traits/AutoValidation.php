<?php

	namespace Signup\Traits;

	use Illuminate\Support\MessageBag;
	use Validator;

	trait AutoValidation {

		/**
		 * Define errors from message bag
		 *
		 * @var array
		 */
		public $errors;
		
		/**
		 * Return errors
		 */
		public function errors() {
			return $this->errors;
		}

		/**
		 * Validate inputs based on rules
		 */
		public function valid($data) {
			/* Set validation based on rules provided */
			$validation = Validator::make($data, $this->rules);

			if($validation->fails()) {
				/* Set errors to validation messages */
				$this->errors = $validation->messages();
				return false;
			}

			/* Return true if validation passed */
			return true;
		}

	}