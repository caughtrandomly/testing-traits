<?php

class BaseController extends Controller {

	/**
	 * Set Constructor
	 *
	 * @return void
	 */
	public function __construct() {
		/* Check POST data */
		$this->beforeFilter('csrf', array('on' => array('post')));
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout() {
		if (!is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}
	}

}
