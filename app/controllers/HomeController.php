<?php

	class HomeController extends BaseController {

		/**
		 * Define signup
		 */
		protected $signup;

		/**
		 * Parent constructor
		 */
		public function __construct(Signup $signup) {
			$this->signup = $signup;
		}

		/**
		 * Return home view
		 *
		 * @return view
		 */
		public function index() {
			return View::make('home.index');
		}

		/**
		 * Store address
		 */
		public function store() {
			/* Set Checking */
			$signup = $this->signup->valid(Input::all());

			/* Check Validation */
			if($signup) {
				/* Insert */
				$s = new Signup;
				$s->email = Input::get('email');
				$s->save();

				/* Redirect with Success */
				return Redirect::to('/')->with('success', 'Signup completed!');
			} else {
				/* Set Error and Redirect */
				$errors = $this->signup->errors();
				return Redirect::to('/')->with('errors', $errors)->withInput();
			}
		}

	}