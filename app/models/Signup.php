<?php

	class Signup extends Eloquent {
		use Signup\Traits\AutoValidation;

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'signups';

		/**
		 * Set rules for form validation.
		 *
		 * @var array
		 */
		public $rules = array(
			'email' => 'required|email|unique:signups'
		);

	}